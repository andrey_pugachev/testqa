package andrey.fortest;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class Ex2 {

    /*
    Задание:
    Создайте текстовый файл, в котором есть значения от 0 до 20 в произвольном порядке (перемешаны).
    Значения указаны через запятую без пробелов.
        ● Прочитайте файл, отфильтруйте значения по возрастанию и выведите
        результат в консоль.
        ● Прочитайте файл, отфильтруйте значения по убыванию и выведите результат в
        консоль.
    */

    private static String name = "tempFile.txt";

    public static void main(String[] args) {
        String str;
        if (generateFile(20)) {
            ArrayList<Integer> list = new ArrayList<>();
            try {
                str = Files.lines(Paths.get(name)).collect(Collectors.joining());
                Arrays.stream(str.split(",")).forEach(s -> list.add(Integer.parseInt(s)));
                Collections.sort(list);
                System.out.println(list.toString());
                Collections.reverse(list);
                System.out.println(list.toString());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.println("Ошибка создания файла");
        }
    }

    private static boolean generateFile(int amount) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i <= amount; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        try (FileWriter writer = new FileWriter(name, false)) {
            writer.write(list.toString().replaceAll("[\\[\\]\\s]", ""));
            writer.flush();
            return true;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }
}
