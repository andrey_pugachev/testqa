package andrey.fortest;

public class Ex3 {
    /*
    Посчитайте факториал 20, и выведите результат в консоль.
    */

    public static void main(String[] args) {
        System.out.println(factorial(20));
    }

    private static long factorial (int x) {
        long result = 1;
        for (int i = 1; i <= x; i++) {
            result *= i;
        }
        return result;
    }
}
